\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{am_cv}[2023/06/22 Custom CV class by Alberto Mosconi]

\LoadClass[11pt]{article}

\RequirePackage[margin=15mm]{geometry}
\RequirePackage{fancyhdr}
\RequirePackage{titlesec}
\RequirePackage{tabularx}
\RequirePackage{enumitem}
\RequirePackage{setspace}
\RequirePackage{wrapfig}
\RequirePackage{graphicx}
\RequirePackage[hidelinks]{hyperref}
\RequirePackage{fontawesome5}
\RequirePackage{Oswald}
\newcommand{\fontoswald}{\fontfamily{Zeroswald-\Zeroswald@figurealign\Zeroswald@figurestyle}\selectfont}
\RequirePackage[sfdefault]{noto}
\RequirePackage{charter}
\RequirePackage[T1]{fontenc}
\RequirePackage{microtype}
\RequirePackage{xifthen}

\setlist{nosep}
\pagestyle{fancy}
\fancyhf{}
\fancyfoot{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\setlength{\parindent}{0mm}
\setlength{\parskip}{0mm}
\setlength{\tabcolsep}{0mm}

\newcommand{\header}[6]{
    \begin{tabularx}{\textwidth}{>{\raggedright\arraybackslash}X>{\raggedleft\arraybackslash}X}
        \textbf{\fontoswald\Huge{#1}} & {\small#2} \\
        {\small#3}                    & {\small#4} \\
        {\small#5}                    & {\small#6}
    \end{tabularx}
    \vspace{4mm}
}

\titleformat{\section}[display]{\raggedright}{}{0mm}{}

\newcommand{\cvsection}[1]{
    \vspace{-12mm}
    \section{\fontoswald\Large{#1}}
    \vspace{-4mm}
    \hrule
    \vspace{2mm}
}

\newcommand{\jobliststart}{
    \begin{itemize}[leftmargin=4mm, rightmargin=0mm]
        \setlength{\itemsep}{2mm}
        }
        \newcommand{\joblistend}{\vspace{4mm}\end{itemize}}

\newcommand{\job}[5]{
    \item
    \begin{tabularx}{\linewidth}[t]{l@{\extracolsep{\fill}}r}
        \textbf{#1}       & #2                 \\
        \textit{\small#3} & \textit{\small #4}
    \end{tabularx}
    \begin{itemize}[leftmargin=4mm, rightmargin=45mm]
        \linespread{.5}\selectfont
        \setlength{\itemsep}{0mm}
        #5
    \end{itemize}
}

\newcommand{\jobdescription}[1]{
    \item[-] {\small #1}
}

\newcommand{\projectliststart}{
    \begin{itemize}[leftmargin=4mm, rightmargin=0mm]
        \linespread{.6}\selectfont
        \setlength{\itemsep}{1mm}
        }

        \newcommand{\projectlistend}{
        \vspace{4mm}\end{itemize}
}

\newcommand{\project}[2]{
\item[-] \textbf{#1}: {\small#2}
\vspace{1mm}
}

\newcommand{\educationliststart}{
    \begin{itemize}[leftmargin=4mm, rightmargin=0mm]
        \setlength{\itemsep}{1mm}
        }

        \newcommand{\educationlistend}{
        \vspace{4mm}\end{itemize}
}

\newcommand{\education}[5]{
    \item
    \begin{tabularx}{\linewidth}[t]{l@{\extracolsep{\fill}}r}
        \textbf{#1}       & #2                 \\
        \textit{\small#3} & \textit{\small #4}
    \end{tabularx}
    \begin{itemize}[leftmargin=4mm, rightmargin=45mm]
        \linespread{.5}\selectfont
        \setlength{\itemsep}{0mm}
        #5
    \end{itemize}
}

\newcommand{\educationsubitem}[2]{
    \item[] {\small\textit{#1}: #2}
}

\newcommand{\footer}[1]{
    \vspace*{\fill}
    \begin{wrapfigure}{r}{0.2\linewidth}
        \vspace{-10mm}
        \def\svgwidth{0.2\textwidth}
        \input{./signature.pdf_tex}
    \end{wrapfigure}
    \footnotesize#1
}

\newcommand{\skillstart}{
    \begin{itemize}[leftmargin=4mm, rightmargin=0mm]
        \small
        }

        \newcommand{\skillend}{
    \end{itemize}
    \vspace{4mm}
}

\newcommand{\skillitem}[2]{
    \item[-] \ifthenelse{\isempty{#1}}{}{\textbf{\normalsize#1}: }#2
}
